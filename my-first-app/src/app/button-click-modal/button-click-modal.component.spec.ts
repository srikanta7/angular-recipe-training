import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonClickModalComponent } from './button-click-modal.component';

describe('ButtonClickModalComponent', () => {
  let component: ButtonClickModalComponent;
  let fixture: ComponentFixture<ButtonClickModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonClickModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonClickModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
