import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { ItemCardsListComponent } from './item-cards-list/item-cards-list.component';
import { ButtonClickModalComponent } from './button-click-modal/button-click-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ItemCardComponent,
    ItemCardsListComponent,
    ButtonClickModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
