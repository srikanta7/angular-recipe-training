import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GalleryViewToggleService {
  private isGalleryViewAsked = new BehaviorSubject(true);
  currentView = this.isGalleryViewAsked.asObservable();
  constructor() {}
  toggleView(isGridViewAsked: boolean) {
    this.isGalleryViewAsked.next(isGridViewAsked);
  }
}
