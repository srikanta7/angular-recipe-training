import { TestBed } from '@angular/core/testing';

import { GalleryViewToggleService } from './gallery-view-toggle.service';

describe('GalleryViewToggleService', () => {
  let service: GalleryViewToggleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GalleryViewToggleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
