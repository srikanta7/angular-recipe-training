import { Component, OnInit } from '@angular/core';
import { GalleryViewToggleService } from '../services/gallery-view-toggle.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  isGridViewRequested:boolean=false;
  constructor(private toggleViewService:GalleryViewToggleService) { 
    this.toggleViewService.currentView.subscribe(view => this.isGridViewRequested = view);

  }

  ngOnInit(): void {
  }
  clickEvent(){
    this.isGridViewRequested=!this.isGridViewRequested;
    this.toggleViewService.toggleView(this.isGridViewRequested);
    console.log(this.isGridViewRequested);
  }

}
