import { Component, OnInit } from '@angular/core';
import { GalleryViewToggleService } from '../services/gallery-view-toggle.service';

@Component({
  selector: 'app-item-cards-list',
  templateUrl: './item-cards-list.component.html',
  styleUrls: ['./item-cards-list.component.css']
})
export class ItemCardsListComponent implements OnInit {

  isGridViewRequested:boolean=false;
  constructor(private toggleService:GalleryViewToggleService) { 
    this.toggleService.currentView.subscribe(view => this.isGridViewRequested = view)
  }

  ngOnInit(): void {
  }

}
