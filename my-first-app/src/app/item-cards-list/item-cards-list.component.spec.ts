import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCardsListComponent } from './item-cards-list.component';

describe('ItemCardsListComponent', () => {
  let component: ItemCardsListComponent;
  let fixture: ComponentFixture<ItemCardsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCardsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCardsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
